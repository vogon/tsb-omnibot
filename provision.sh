apt-get -y update

# su to vagrant
sudo -u vagrant /bin/bash - <<eof
# set up rvm
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
source /home/vagrant/.rvm/scripts/rvm

rvm install ruby-2.2.1
rvm --default use 2.2.1

# set up gems
gem install bundler
eof
# back to root

# set up git
apt-get -y install git

# # set up postgres
# apt-get -y install postgresql libpq-dev

# # set up postgres server
# sudo -u postgres /bin/bash - <<eof
# createuser --superuser vagrant
# createdb redcent
# eof
# # back to root

# set up heroku toolbelt
wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh