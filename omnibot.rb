require 'json'
require 'open-uri'

require 'slack'

OWNER = 'U03QFK51N'
DEV_CHANNEL = 'C040VELN6'

$production = true
$bot_user = nil

$cards = []
$cards_by_nrdb_id = {}

$commands = {}
$message_responders = {}

def reload_behaviors
	load './behaviors.rb'
end

def cmd_reload(slack, message, args)
	if message['user'] != OWNER
		# don't let normal users change operation mode
		return
	end

	slack.chat_postMessage(as_user: true, channel: message['channel'], text: "reloading behaviors...")
	reload_behaviors
	slack.chat_postMessage(as_user: true, channel: message['channel'], text: "done.")
end

reload_behaviors

def do_message(slack, data)
	# do every-message message processing
	$message_responders.each do |responder|
		responder.call(slack, data)
	end

	# do bang-command-style message processing
	ts = data['ts']
	command, args = data['text'].split($;, 2)

	handler = $commands[command]

	if handler != nil
		handler.call(slack, data, args)
	end
end

# reload silently when bot starts
do_message(nil, {'text' => '!update silent', 'user' => OWNER})

Slack.configure do |config|
	config.token = ENV['SLACK_TOKEN']
end

slack = Slack::Client.new
$bot_user = slack.auth_test['user_id']

rt = slack.realtime

rt.on(:message) do |data|
	channel = data['channel']

	if !$production && DEV_CHANNEL != channel
		next
	end

	if data['text'] == nil
		next
	end

	do_message(slack, data)

	# p slack.chat_update(
	# 		ts: ts,
	# 		channel: channel,
	# 		text: original_text + ' (bot edited)'
	# 	)
end

rt.start