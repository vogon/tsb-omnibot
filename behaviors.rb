require 'cgi'
require 'open-uri'
require 'slack'

RUNNER_FACTIONS = {
	'adam' => { :name => 'Adam', :color => '#b9b23a'},
	'anarch' => { :name => 'Anarch', :color => '#ff4500' },
	'apex' => { :name => 'Apex', :color => '#9e564e' },
	'criminal' => { :name => 'Criminal', :color => '#4169e1' },
	'shaper' => { :name => 'Shaper', :color => '#32cd32' },
	'sunny-lebeau' => { :name => 'Sunny Lebeau', :color => '#715778' },
	'neutral' => { :name => 'Neutral (runner)', :color => '#808080' }
}

CORP_FACTIONS = {
	'haas-bioroid' => { :name => 'Haas-Bioroid', :color => '#8a2be2' },
	'jinteki' => { :name => 'Jinteki', :color => '#dc143c' },
	'nbn' => { :name => 'NBN', :color => '#ff8c00' },
	'weyland-consortium' => { :name => 'Weyland Consortium', :color => '#326b5b' },
	'neutral' => { :name => 'Neutral (corp)', :color => '#808080' }
}

def nrdb_rules_text_to_slack_markup(text)
	if !text
		return ''
	else
		return text.
			# replace credits and clicks with our emoji
			gsub('[Credits]', ':credit:').
			gsub('[Click]', ':click:').
			gsub('[Trash]', ':trash:').
			gsub('[Recurring Credits]', ':recurcredit:').
			gsub('[Subroutine]', ':sub:').
			gsub('[Memory Unit]', ':mu:').
			gsub('[Link]', ':anr-link:').
			# replace superscripts with a leading space ("trace^5" becomes "trace 5")
			gsub('<sup>', ' ').
			gsub('</sup>', '').
			# replace &ndash; with an en dash character
			gsub('&ndash;', " \u2013").
			# replace <strong> tags with markdown
			gsub('<strong>', '*').
			gsub('</strong>', '*')
	end
end

def nrdb_card_to_attachment(card)
	# fancy up the title if the card is unique
	formatted_title = card['title']

	if card['uniqueness']
		formatted_title = "\u2756 #{card['title']}"
	end

	attachment = {
		'fallback': formatted_title,
		'title': formatted_title,
		'title_link': card['url'],
		'mrkdwn_in': ['text']
	}

	# generate attachment text
	attachment['text'] = ''

	# make typeline
	typeline = ''

	if card['subtype'] != nil && card['subtype'] != ''
		typeline += "*#{card['type']}*: #{card['subtype']}"
	else
		typeline += "*#{card['type']}*"
	end

	# dig up type-specific fields
	case card['type_code']
	when 'agenda' 
		typeline += " _(advancement requirement #{card['advancementcost']}, #{card['agendapoints']} agenda points)_"
	when 'asset', 'upgrade'
		typeline += " _(rez cost #{card['cost']}, trash cost #{card['trash']})_"
	when 'event', 'operation' 
		typeline += " _(play cost #{card['cost']})_"
	when 'hardware', 'resource'
		typeline += " _(install cost #{card['cost']})_"
	when 'ice'
		typeline += " _(rez cost #{card['cost']}, strength #{card['strength']})_"
	when 'identity'
		if card['side_code'] == 'runner'
			typeline += " _(#{card['baselink']}:anr-link:, deck size #{card['minimumdecksize']}, influence #{card['influencelimit']})_"
		elsif card['side_code'] == 'corp'
			typeline += " _(deck size #{card['minimumdecksize']}, influence #{card['influencelimit']})_"
		end
	when 'program'
		if card['strength'] != nil
			typeline += " _(install cost #{card['cost']}, #{card['memoryunits']}:mu:, strength #{card['strength']})_"
		else
			typeline += " _(install cost #{card['cost']}, #{card['memoryunits']}:mu:)_"
		end
	end

	attachment['text'] += typeline + "\n\n"
	attachment['text'] += nrdb_rules_text_to_slack_markup(card['text'])

	if card['flavor'] != nil && card['flavor'] != ''
		attachment['text'] += "\n\n"

		card['flavor'].each_line do |line|
			attachment['text'] += " _#{line.strip}_\n"
		end
	end

	# add faction flair
	if card['side_code'] == 'corp'
		faction = CORP_FACTIONS[card['faction_code']]

		if faction
			if card['factioncost'] != nil
				attachment['author_name'] = "#{card['setname']} / #{faction[:name]} (#{card['factioncost']} influence)"
			else
				attachment['author_name'] = "#{card['setname']} / #{faction[:name]}"
			end
			
			attachment['color'] = faction[:color]
		end
	elsif card['side_code'] == 'runner'
		faction = RUNNER_FACTIONS[card['faction_code']]

		if faction
			if card['factioncost'] != nil
				attachment['author_name'] = "#{card['setname']} / #{faction[:name]} (#{card['factioncost']} influence)"
			else
				attachment['author_name'] = "#{card['setname']} / #{faction[:name]}"
			end

			attachment['color'] = faction[:color]
		end
	end

	return attachment
end

def nrdb_decklist_to_attachment(decklist, url)
	attachment = {
		'fallback': decklist['name'],
		'title': decklist['name'],
		'title_link': url,
		'mrkdwn_in': ['text']
	}

	attachment['text'] = ''

	# find identity
	identity = $cards_by_nrdb_id[decklist['identity_code']]

	if identity
		attachment['text'] += "*Identity: <#{identity['url']}|#{identity['title']}>*"
	end

	# add all the slots
	decklist['slots'].each_pair do |nrdb_id, count|
		card = $cards_by_nrdb_id[nrdb_id]

		if card
			attachment['text'] += "\n#{count}x <#{card['url']}|#{card['title']}>"
		end
	end

	# add faction flair
	faction_code = nil

	if decklist['faction_code'] == 'neutral'
		faction_code = '-'
	else
		faction_code = decklist['faction_code']
	end

	if decklist['side_name'] == 'corp'
		faction = CORP_FACTIONS[faction_code]

		if faction
			attachment['color'] = faction[:color]
		end
	elsif decklist['side_name'] == 'runner'
		faction = RUNNER_FACTIONS[faction_code]

		if faction
			attachment['color'] = faction[:color]
		end
	end

	return attachment
end

def cmd_update(slack, message, args)
	if message['user'] != OWNER
		# don't let normal users spam NRDB
		return
	end

	silent = false

	if args =~ /^silent/
		silent = true
	end

	if !silent
		update_msg = slack.chat_postMessage(as_user: true, channel: message['channel'], text: "updating card data from NRDB...")
	end

	open('https://netrunnerdb.com/api/cards/') do |f|
		new_cards = JSON.parse(f.read)

		$cards = new_cards
		$cards_by_nrdb_id = {}

		new_cards.each do |new_card|
			$cards_by_nrdb_id[new_card['code']] = new_card
		end

		if !silent
			slack.chat_postMessage(as_user: true, channel: message['channel'], text: "#{new_cards.count} cards loaded.")
		end
	end
end

def find_cards(name)
	# remove HTML entities from search query
	name = CGI.unescapeHTML(name)

	matching_cards = []

	$cards.each do |card|
		match_start = card['title'].downcase.index(name)

		# skip cards that don't match the search query at all
		next if match_start == nil

		# skip cards that have the search string as part of a word
		next if match_start > 0 && card['title'][match_start - 1] != ' '
		next if (match_start + name.length) < card['title'].length && card['title'][match_start + name.length] != ' '

		matching_cards << card
	end

	return matching_cards
end

def say_cards(slack, message_text, cards, channel)
	slack.chat_postMessage(
		as_user: true,
		channel: channel,
		text: message_text,
		attachments: cards.map { |card| nrdb_card_to_attachment(card) }.to_json)
end

def cmd_card(slack, message, args)
	if args == nil || args.strip == ''
		slack.chat_postMessage(
			as_user: true,
			channel: message['channel'], 
			text: "<@#{message['user']}>: use !card by typing '!card' and then the name of a card."
			)
		return
	end

	lc_search = args.downcase.strip
	matching_cards = find_cards(lc_search)

	if matching_cards.empty?
		slack.chat_postMessage(
			as_user: true,
			channel: message['channel'],
			text: "<@#{message['user']}>: I couldn't find any cards called '#{lc_search}'."
			)
		return
	else
		say_cards(slack, "<@#{message['user']}>:", matching_cards, message['channel'])
	end
end

def cmd_dev_prod(slack, message, args, production)
	if message['user'] != OWNER
		# don't let normal users change operation mode
		return
	end

	$production = production
	mode_name = production ? 'production' : 'development'

	slack.chat_postMessage(as_user: true, channel: message['channel'], text: "<@#{message['user']}>: switched to #{mode_name} mode.")
end

def respond_card_refs(slack, message)
	card_ref_re = /\[([^\]]*)\]/

	message['text'].scan(card_ref_re) do |match|
		# iterate through card references in the message
		search = $1.downcase.strip
		matching_cards = find_cards(search)

		if matching_cards.empty?
			slack.chat_postMessage(
				as_user: true,
				channel: message['channel'],
				text: "<@#{message['user']}>: I couldn't find any cards called '#{search}'."
				)
			return
		else
			say_cards(slack, "'#{search}' matches:", matching_cards, message['channel'])
		end
	end
end

def respond_nrdb_deck_viewer_links(slack, message)
	deck_viewer_link_re = %r|<(https?://netrunnerdb.com/\w+/deck/view/\d+)>|

	if message['text'] =~ deck_viewer_link_re

		url = $~[1]
		# puts "nrdb deck viewer url found: #{url}"

		open(url) do |f|
			# terrible hack RE to extract the decklist from the deck viewer JS block
			deck_extractor_re = /Deck\s+=\s+(.*),\s+DeckDB/

			if f.read =~ deck_extractor_re
				decklist = JSON.parse($~[1])
				attachments =  [nrdb_decklist_to_attachment(decklist, url)].to_json

				# p attachments

				slack.chat_postMessage(
					as_user: true,
					channel: message['channel'],
					text: ' ',
					attachments: attachments
					)
			end
		end
	end
end

$commands = {
	'!update' => method(:cmd_update),
	'!card' => method(:cmd_card),
	'!dev' => lambda { |slack, message, args| cmd_dev_prod(slack, message, args, false) },
	'!prod' => lambda { |slack, message, args| cmd_dev_prod(slack, message, args, true) },
	'!reload' => method(:cmd_reload)
}

$message_responders = [
	method(:respond_card_refs),
	method(:respond_nrdb_deck_viewer_links)
]